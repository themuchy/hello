FROM node:15-alpine
ENV NODE_ENV=production
COPY ["app.js", "package.json", "package-lock.json*", "./"]
RUN npm install --production
RUN npm install cors
CMD ["node", "app.js"]
